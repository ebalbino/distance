// @flow
import * as React from 'mangojuice-react';
import { Grid, Row, Col } from 'react-bootstrap';
import * as Model from './Model';
import LogicClass from './Logic';
import * as DistanceForm from '../DistanceForm';
import * as DistanceResult from '../DistanceResult';

// Types
type Props = { model: Model.Model };
type Context = { Logic: LogicClass };

// Views
const AppPageView = ({ model }: Props, { Logic }: Context) => (
  <div className="container">
    <h2>Distance Search</h2>
    <DistanceForm.View model={model.form} />
    <DistanceResult.View model={model.result} />
  </div>
);

export default AppPageView;
