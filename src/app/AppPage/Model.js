// @flow
import * as DistanceForm from '../DistanceForm';
import * as DistanceResult from '../DistanceResult';

export type FactoryProps = {
};

export type Model = {
  form: DistanceForm.Model,
  result: DistanceResult.Model
};

export const createModel = (props: FactoryProps = {}): Model => {
  const params =  new URLSearchParams(location.search);
  const origin = params.get('origin');
  const destination = params.get('destination');
  let form_props = {};
  if (origin && destination) {
      form_props = {origin, destination};
  }
  return {
      form: DistanceForm.createModel(form_props),
      result: DistanceResult.createModel()
  };
};

