// @flow
import type { Model } from './Model';
import { LogicBase, Command, cmd, child, logicOf, depends } from 'mangojuice-core';
import * as DistanceForm from '../DistanceForm';
import * as DistanceResult from '../DistanceResult';

export default class AppPage extends LogicBase<Model> {
  children() {
    return {
      form: DistanceForm.Logic,
      result: DistanceResult.Logic
    };
  }

  hubAfter(cmd: Command) {
    if (cmd.is(logicOf(this.model.form).Search)) {
        return logicOf(this.model.result).Search(this.model.form.origin, this.model.form.destination);
    }
    if (this.model.form && this.model.destination) {
        console.log("In here!");
        return logicOf(this.model.result).Search(this.model.form.origin, this.model.form.destination);
    }
  }
}
