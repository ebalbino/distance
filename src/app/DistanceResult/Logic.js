// @flow
import * as Model from './Model';
import { LogicBase, cmd, child, task } from 'mangojuice-core';
import * as Tasks from './Tasks';

export default class DistanceResult extends LogicBase<Model.Model> {
  port(exec: Function, destroyed: Promise<void>) {
    const timer = setInterval(() => {
      exec(this.Search(this.model.origin, this.model.destination));
    }, 10000);
    destroyed.then(() => clearInterval(timer));
  }

  @cmd Search(origin: string, destination: string) {
    return [
      this.InitSearch(origin, destination),
      this.DoSearch()
    ];
  }

  @cmd DoSearch() {
    return task(Tasks.findDistance)
      .success(this.SetDistance)
      .fail(this.HandleSearchFail);
  }

  @cmd InitSearch(origin: string, destination: string) {
    return { origin, destination, loading: true };
  }

  @cmd SetDistance(result: any) {
    return {
      distance: result * 0.000621371,
      loading: false
    };
  }

  @cmd HandleSearchFail(err: any) {
    return {
      error: err && err.message || 'Some unkonwn error happened',
      results: [],
      loading: false
    };
  }
}
