// @flow

export type FactoryProps = {
};

export type Model = {
  origin: string,
  destination: string,
  loading: bool,
  error: string,
  distance: number,
};

// Utils
export const createModel = (props: FactoryProps = {}): Model => ({
  results: [],
  origin: '',
  destination: '',
  loading: false,
  error: '',
});
