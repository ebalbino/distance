// @flow
import * as Model from './Model';

// Types
type Props = { model: Model.Model };

// Utils
const fetch = window.fetch.bind(window);

/**
 * Searches distance between two locations by given origin and destination and returns distance
 * search items
 * @param  {Props} props
 * @return {Promise}
 */
export async function findDistance({ model }: Props): Promise<string> {
    // Uses Cors-Anywhere because the service doesn't allow client side apps to retrieve data from it.
  const { result, error } = await this.call(fetch,
      `https://cors-anywhere.herokuapp.com/https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=${model.origin}&destinations=${model.destination}&key=AIzaSyAmKhrlBDfB-MlN75ElFGaMaa0nyQRTK4Y`);

  if (error) {
    throw error;
  }

  const json = await result.json();
  return json.rows[0].elements[0].distance.value;
}
