// @flow
import * as Model from './Model';
import * as React from 'mangojuice-react';
import Logic from './Logic';
import { Grid, Row, Col, Panel } from 'react-bootstrap';

type Props = { model: Model.Model };

const DistanceResultView = ({ model }: Props) => {
  // Return error immediate if there is one
  if (model.error) {
    return (
      <Panel bsStyle="danger">
        <Panel.Heading>
        <Panel.Title>Error!</Panel.Title>
        </Panel.Heading>
        <Panel.Body>
          <p className="text-center">
            It appears there was an error. Maybe you typed in the name of a city incorrectly.
          </p>
        </Panel.Body>
      </Panel>
    );
  }

  // Checks if result has been loaded
  const loaded = !model.loading && model.distance > 0;
  if (loaded) {
    return (
      <Panel bsStyle="success">
        <Panel.Heading>
        <Panel.Title>Result Found!</Panel.Title>
        </Panel.Heading>
        <Panel.Body>
          <p className="text-center">
            From <strong>{model.origin}</strong> to <strong>{model.destination}</strong> is <strong>{model.distance} miles</strong>
          </p>
        </Panel.Body>
      </Panel>
    );
  } else if (model.loading) {
    // Renders loading text to indicate that results are still being pulled
    return (
      <h3 className="text-center">Loading result...</h3>
    );
  } 

  // Null and thus invisibile to user until search command is fired by user.
  return null;
};

export default DistanceResultView;
