// @flow
import * as Model from './Model';
import * as React from 'mangojuice-react';
import LogicClass from './Logic';
import { Grid, Row, Col, Panel, FormGroup, FormControl, ControlLabel, Button } from 'react-bootstrap';

type Props = { model: Model.Model };
type Context = { Logic: LogicClass };

const DistanceFormView = ({ model }: Props, { Logic }: Context) => (
  <div>
    <Panel bsStyle="primary">
      <Panel.Body>
        <FormGroup controlId="formOrigin">
          <ControlLabel>Origin</ControlLabel>
          <FormControl
              type="text"
              value={model.origin}
              placeholder="Enter origin"
              onChange={Logic.SetOrigin}/>
        </FormGroup>
        <FormGroup controlId="formDestination">
          <ControlLabel>Destination</ControlLabel>
          <FormControl
              type="text"
              value={model.destination}
              placeholder="Enter destination"
              onChange={Logic.SetDestination}/>
        </FormGroup>
      <Button onClick={Logic.Search}>Search Distance</Button>
      </Panel.Body>
    </Panel>
  </div>
);

export default DistanceFormView;
