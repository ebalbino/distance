// @flow
import type { Model } from './Model';
import { LogicBase, cmd, child } from 'mangojuice-core';


export default class DistanceForm extends LogicBase<Model> {
  @cmd SetOrigin(e: string) {
    return { origin: e.target.value };
  }

  @cmd SetDestination(e: string) {
    return { destination: e.target.value };
  }

  @cmd Search() {
  }
}
