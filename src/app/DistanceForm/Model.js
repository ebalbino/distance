// @flow

export type FactoryProps = {
};

export type Model = {
  origin: string,
  destination: string
};

export const createModel = (props: FactoryProps = {}): Model => ({
  origin: props.origin ? props.origin : '',
  destination: props.destination ? props.destination : ''
});
