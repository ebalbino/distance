# MangoJuice Distance Search
This is a small web application for calculating the distance between two locations using the Google Distance Matrix API for calculating distances, React for view rendering and MangoJuice for state management. 

## Features
- Can use query parameters origin and destination to for deeplinking e.g. (/?origin=Los+Angeles&destination=New+York)

## How to run?
```sh
npm install
npm run start
```

```sh
yarn install
yarn start
```


